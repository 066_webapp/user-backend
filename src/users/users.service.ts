import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

let users: User[] = [
  { id: 1, login: 'admin', name: 'Administrator', password: 'Pass@1234' },
  { id: 2, login: 'user1', name: 'User1', password: 'Pass@1234' },
  { id: 3, login: 'user2', name: 'User2', password: 'Pass@1234' },
];

let lastUserId = 4;
@Injectable()
export class UsersService {
  create(createUserDto: CreateUserDto) {
    console.log({ ...createUserDto });
    const newUser: User = {
      id: lastUserId++,
      ...createUserDto, //login name password
    };
    // const newUser = new User();
    // newUser.id = lastUserId++;
    // newUser.login = createUserDto.login;
    // newUser.name = createUserDto.name;
    // newUser.password = createUserDto.password;
    users.push(newUser);
    return newUser;
    // return 'This action adds a new user';
  }

  findAll() {
    return users;
    // return `This action returns all users`;
  }

  findOne(id: number) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return users[index];
    // return `This action returns a #${id} user`;
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log('user' + JSON.stringify(users[index]));
    // console.log('update' + JSON.stringify(updateUserDto));
    const updateUser: User = {
      ...users[index],
      ...updateUserDto,
    };
    users[index] = updateUser;
    return updateUser;
    // return `This action updates a #${id} user`;
  }

  remove(id: number) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteUser = users[index];
    users.splice(index, 1);
    return deleteUser;
    // return `This action removes a #${id} user`;
  }

  reset() {
    users = [
      { id: 1, login: 'admin', name: 'Administrator', password: 'Pass@1234' },
      { id: 2, login: 'user1', name: 'User1', password: 'Pass@1234' },
      { id: 3, login: 'user2', name: 'User2', password: 'Pass@1234' },
    ];
    lastUserId = 4;
    return 'RESET';
  }
}
