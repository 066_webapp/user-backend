import { IsNotEmpty, MinLength, IsInt, IsPositive } from 'class-validator';
export class CreateProductDto {
  @MinLength(8)
  @IsNotEmpty()
  name: string;
  @IsPositive()
  @IsInt()
  @IsNotEmpty()
  price: number;
}
