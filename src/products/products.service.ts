import { Product } from './entities/product.entity';
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';

let products: Product[] = [
  { id: 1, name: 'Friedrice', price: 40 },
  { id: 2, name: 'Coconut Shake', price: 30 },
  { id: 3, name: 'Choco Cookies', price: 30 },
];

let lastId = 4;

@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastId++,
      ...createProductDto,
    };
    products.push(newProduct);
    return newProduct;
    // return 'This action adds a new product';
  }

  findAll() {
    return products;
    // return `This action returns all products`;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
    // return `This action returns a #${id} product`;
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
    // return `This action updates a #${id} product`;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteProduct = products[index];
    products.splice(index, 1);
    return deleteProduct;
    // return `This action removes a #${id} product`;
  }

  reset() {
    products = [
      { id: 1, name: 'Friedrice', price: 40 },
      { id: 2, name: 'Coconut Shake', price: 30 },
      { id: 3, name: 'Choco Cookies', price: 30 },
    ];

    lastId = 4;

    return 'RESET';
  }
}
